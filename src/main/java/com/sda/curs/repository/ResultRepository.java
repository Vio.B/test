package com.sda.curs.repository;

import com.sda.curs.entity.Result;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultRepository extends PagingAndSortingRepository<Result, Integer> {
    Result findById(int id);
    Result findByTest_Id(int testId);
    Result findByUser_Id(int userId);
}
