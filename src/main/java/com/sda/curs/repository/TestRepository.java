package com.sda.curs.repository;

import com.sda.curs.entity.Test;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TestRepository extends PagingAndSortingRepository<Test, Integer> {
    Test findById(int id);

}
