package com.sda.curs.repository;

import com.sda.curs.entity.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleRepository extends PagingAndSortingRepository<Role, Integer> {
    Role findById(int id);
}
