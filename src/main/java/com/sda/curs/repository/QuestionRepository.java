package com.sda.curs.repository;

import com.sda.curs.entity.Question;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends PagingAndSortingRepository<Question, Integer> {
    Question findById(int id);
    List<Question> findAllByTest_Id(int testId);
}
