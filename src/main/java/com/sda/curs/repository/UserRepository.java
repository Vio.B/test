package com.sda.curs.repository;

import com.sda.curs.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Integer> {
    List<User> findByFirstName(String firstName);
    User findById(int id);
    User findByUsername(String username);
}
