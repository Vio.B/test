package com.sda.curs.controller;

import com.sda.curs.entity.Question;
import com.sda.curs.service.QuestionService;
import com.sda.curs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("api/v1/questions")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private UserService userService;

    @GetMapping("/getAllQuestionsFromATest/{id}")
    public String findAllByTest(@PathVariable(value = "id") int id, Model model, @AuthenticationPrincipal UserDetails currentUser) {
        model.addAttribute("getAllQuestions", questionService.findAllByTest_id(id));
        model.addAttribute("testId", id);
        model.addAttribute("currentUser", userService.loadUserByUsername(currentUser.getUsername()));
        model.addAttribute("currentUser2", userService.findByUsername(currentUser.getUsername()));
        return "getAllQuestions";
    }

    @PostMapping("/create")
    public ResponseEntity create(@RequestBody Question question) {
        return ResponseEntity.ok(questionService.create(question));
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam("Id") int id) {
        questionService.delete(id);
    }

    @GetMapping("/findById")
    public Question findById(@RequestParam("Id") int id) {
        return questionService.findById(id);
    }

}
