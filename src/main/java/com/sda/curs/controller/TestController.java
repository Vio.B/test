package com.sda.curs.controller;

import com.sda.curs.entity.Test;
import com.sda.curs.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/api/v1/tests")
@ControllerAdvice
public class TestController {

    @Autowired
    private TestService testService;

    @GetMapping("/getAllTests")
    public String getAllTests(Model model) {
        model.addAttribute("getAllTests", testService.getAllTests());
        return "getAllTests";
    }

    @GetMapping("/showFormForUpdate/{id}")
    public String showFormForUpdate(@PathVariable(value = "id") int id, Model model){
         Test test = testService.findById(id);
         model.addAttribute("test", test);
         return "updateTest";
    }

    @GetMapping("/newTest")
    public String newTest(Model model) {
        Test test = new Test();
        model.addAttribute("test", test);
        return "createTest";
    }

    @PostMapping("/saveTest")
    public RedirectView create(@ModelAttribute("test") Test test) {
        this.testService.create(test);
        return new RedirectView("/api/v1/tests/getAllTests");
    }

    @GetMapping("/findById")
    public ResponseEntity findById(@RequestParam("Id") int id) {
        return ResponseEntity.ok(testService.findById(id));
    }

    @GetMapping("/delete/{Id}")
    public RedirectView delete(@PathVariable(value = "Id") int id) {
        System.out.println("Here!");
        testService.delete(id);
        return new RedirectView("/api/v1/tests/getAllTests");
    }
}
