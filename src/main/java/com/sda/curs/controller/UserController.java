package com.sda.curs.controller;

import com.sda.curs.entity.User;
import com.sda.curs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/getAllUsers")
    public String getAllUsers(Model model){
        model.addAttribute("allUsers",userService.getAll());
        return "getAllUsers";
    }

    @GetMapping("/findById")
    public ResponseEntity<User> findById(@RequestParam("Id") int id){
        return ResponseEntity.ok(userService.findById(id));
    }

    @GetMapping("/findByFirstName")
    public ResponseEntity findByFirstName(@RequestParam("firstName") String firstName){
        return ResponseEntity.ok(userService.findByFirstName(firstName));
    }

    @GetMapping("findByUsername")
    public String findByUsername(Model model, @AuthenticationPrincipal UserDetails currentUser){
        User user = userService.findByUsername(currentUser.getUsername());
        model.addAttribute("currentUser", user);
        return "getAllQuestions";
    }

    @GetMapping("loadUserByUsername/{username}")
    public ResponseEntity loadUserByUsername(@PathVariable("username") String username){
        return ResponseEntity.ok(userService.loadUserByUsername(username));
    }
}
