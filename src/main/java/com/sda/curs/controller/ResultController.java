package com.sda.curs.controller;

import com.sda.curs.entity.Result;
import com.sda.curs.entity.Test;
import com.sda.curs.entity.User;
import com.sda.curs.service.ResultService;
import com.sda.curs.service.TestService;
import com.sda.curs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/api/v1/results")
public class ResultController {

    @Autowired
    private ResultService resultService;

    @Autowired
    private TestService testService;

    @Autowired
    private UserService userService;

    //    @PostMapping("/save")
//    @ResponseBody
//    @RequestMapping(value="/save", method=RequestMethod.POST, consumes= MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody//    private RedirectView create(@RequestParam Double score, @RequestParam Integer testId, @RequestParam Integer userId) {
//    private void create(@RequestBody Optional<String> employee) {
//        Test test = testService.findById(testId);
//        User user = userService.findById(userId);
//        Result result = new Result(score, test, user);
//        Result result = new Result();
//        ResponseEntity.ok(resultService.create(result));
//        return new RedirectView("/api/v1/tests/getAllTests");
//    }

    @RequestMapping(value="/save", method=RequestMethod.POST, consumes= MediaType.APPLICATION_JSON_VALUE)
    //@RequestParam(value = "testId") Integer testId, @RequestParam(value = "userId") Integer userId, @RequestParam(value = "score") Integer score
    public RedirectView save() {
//        Test test = testService.findById(testId);
//        User user = userService.findById(userId);
//        Result result = new Result(score, test, user);
//        ResponseEntity.ok(resultService.create(result));
//        System.out.println(score);
        System.out.println(15);
//        System.out.println(userId);
        return new RedirectView("/api/v1/tests/getAllTests");
    }

    @GetMapping("findById")
    public Result findById(@RequestParam("Id") int id) {
        return resultService.findById(id);
    }


}
