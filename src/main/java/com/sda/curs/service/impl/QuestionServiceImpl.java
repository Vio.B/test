package com.sda.curs.service.impl;

import com.sda.curs.entity.Question;
import com.sda.curs.repository.QuestionRepository;
import com.sda.curs.repository.TestRepository;
import com.sda.curs.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private TestRepository testRepository;

    @Override
    public Question create(Question question) {
        Question result = questionRepository.save(question);
        result.setTest(testRepository.findById(result.getTest().getId()).get());
        return result;
    }

    @Override
    public List<Question> findAllByTest_id(int testId) {
        return questionRepository.findAllByTest_Id(testId);
    }

    @Override
    public void delete(int id) {
        questionRepository.deleteById(id);
    }

    @Override
    public Question findById(int id) {
        return questionRepository.findById(id);
    }

}
