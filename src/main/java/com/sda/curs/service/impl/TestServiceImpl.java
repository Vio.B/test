package com.sda.curs.service.impl;

import com.sda.curs.entity.Test;
import com.sda.curs.repository.TestRepository;
import com.sda.curs.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private TestRepository testRepository;

    @Override
    public Test create(Test test) {
        return testRepository.save(test);
    }

    @Override
    public Test findById(int id) {
        return testRepository.findById(id);
    }

    @Override
    public void delete(int id) {
        testRepository.deleteById(id);
    }

    @Override
    public List<Test> getAllTests() {
        return (List<Test>) testRepository.findAll();
    }
}
