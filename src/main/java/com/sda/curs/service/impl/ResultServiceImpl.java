package com.sda.curs.service.impl;
import com.sda.curs.entity.Result;
import com.sda.curs.repository.ResultRepository;
import com.sda.curs.repository.TestRepository;
import com.sda.curs.repository.UserRepository;
import com.sda.curs.service.ResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResultServiceImpl implements ResultService {
    @Autowired
    private ResultRepository resultRepository;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Result create(Result result) {
        Result result1 = resultRepository.save(result);
//        result1.setTest(testRepository.findById(result.getTest().getId()).get());
//        result1.setUser(userRepository.findById(result.getUser().getId()).get());
        return result1;
    }

    @Override
    public Result findById(int id) {
        return resultRepository.findById(id);
    }

    @Override
    public Result findByTestId(int testId) {
        return resultRepository.findByTest_Id(testId);
    }

    @Override
    public Result findByUser_Id(int userId) {
        return null;
    }
}
