package com.sda.curs.service.impl;

import com.sda.curs.dto.UserRegistrationDto;
import com.sda.curs.entity.Role;
import com.sda.curs.entity.User;
import com.sda.curs.repository.RoleRepository;
import com.sda.curs.repository.UserRepository;
import com.sda.curs.service.ResultService;
import com.sda.curs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ResultService resultService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public User create(UserRegistrationDto registrationDto) {
        User user = new User(
                registrationDto.getUsername(),
                registrationDto.getFirstName(),
                registrationDto.getLastName(),
                passwordEncoder.encode(
                        registrationDto.getPassword()),
                Arrays.asList(roleRepository.findById(1)));
        //Role should be changed later
//        Result result = new Result();
//        result.setUser(user);
        return userRepository.save(user);
    }

    @Override
    public void delete(int id) {
        userRepository.deleteById(id);
    }

    @Override
    public User findById(int id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> findByFirstName(String firstName) {
        return userRepository.findByFirstName(firstName);
    }

    @Override
    public List<User> getAll() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password");
        }

        return new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(),
                mapRolesToAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getRole())).collect(Collectors.toList());
    }
}
