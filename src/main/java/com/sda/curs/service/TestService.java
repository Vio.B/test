package com.sda.curs.service;

import com.sda.curs.entity.Test;
//import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;

public interface TestService {
    Test create(Test test);
    Test findById(int id);
    public void delete(int id);
    List<Test>getAllTests(
    );

}
