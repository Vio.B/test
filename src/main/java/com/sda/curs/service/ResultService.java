package com.sda.curs.service;

import com.sda.curs.entity.Result;
import com.sda.curs.entity.Test;
import com.sda.curs.entity.User;

import java.util.List;

public interface ResultService {
    Result create(Result result);
    Result findById(int id);
    Result findByTestId(int testId);
    Result findByUser_Id(int userId);
}
