package com.sda.curs.service;

import com.sda.curs.dto.UserRegistrationDto;
import com.sda.curs.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService  extends UserDetailsService {

    User create(UserRegistrationDto userRegistrationDto);
    void delete(int id);
    User findById(int id);
    List<User> findByFirstName(String firstName);
    List<User> getAll();
    User findByUsername(String username);

}
