package com.sda.curs.service;

import com.sda.curs.entity.Question;
import java.util.List;


public interface QuestionService {
    Question create(Question question);
    void delete(int id);
    Question findById(int id);
    List<Question> findAllByTest_id(int testId);
}
