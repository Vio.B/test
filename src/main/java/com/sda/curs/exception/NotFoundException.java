package com.sda.curs.exception;

public class NotFoundException extends RuntimeException {
    //Whenever the record is not in DB

    //What's serial version UID
    private static final long serialVersionUID = 1L;

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
