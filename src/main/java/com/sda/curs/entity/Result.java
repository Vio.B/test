package com.sda.curs.entity;

import javax.persistence.*;

@Entity
public class Result {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private double score;

    @ManyToOne()
    @JoinColumn(name = "test_id")
    private Test test;

    @ManyToOne()
    @JoinColumn(name = "user_id")
    private User user;

    public Result(){}

    public Result(Test test) {
        this.test = test;
    }

    public Result(int score, Test test) {
        this.score = score;
        this.test = test;
    }

    public Result(double score, Test test, User user){
        this.score = score;
        this.test = test;
        this.user = user;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public double getScore() {
        return score;
    }

    public Test getTest() {
        return test;
    }

    public User getUser() {
        return user;
    }
}
